/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Dao;

import DatabaseHelper.DatabaseHelper;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import model.BestSell;
import model.Bill;

/**
 *
 * @author ASUS
 */
public class BillDao implements Dao<Bill> {

    @Override
    public Bill get(int id) {
        Bill bill = null;
        String sql = "SELECT * FROM check_stock WHERE cs_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                bill = bill.fromRS(rs);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return bill;
    }

    public Bill getByTel(String Tel) {
        Bill customer = null;
        String sql = "SELECT * FROM customer WHERE cus_tel LIKE ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, "%" + Tel + "%");
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                customer = Bill.fromRS(rs);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return customer;
    }

    public String getSumPricebyDay(String date) {
        Bill customer = null;
        String sql = "SELECT SUM(bill_total-bill_discount) as sum_price FROM bill WHERE bill_date LIKE ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, date);
            ResultSet rs = stmt.executeQuery();
            return rs.getString("sum_price");

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return null;
    }

    public int getKey() {
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet key = stmt.getGeneratedKeys();
            key.next();
            return key.getInt(1);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return 0;

    }

    @Override
    public List<Bill> getAll() {
        ArrayList<Bill> list = new ArrayList();
        String sql = "SELECT * FROM customer";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Bill customer = Bill.fromRS(rs);
                list.add(customer);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<Bill> getAllByDate(String date) {
        ArrayList<Bill> list = new ArrayList();
        String sql = "SELECT * FROM bill WHERE bill_date LIKE ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, date);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                Bill bill = Bill.fromRS(rs);
                list.add(bill);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<BestSell> getAllBestsellByDate(String date) {
        ArrayList<BestSell> list = new ArrayList();
        String sql = "SELECT bill_product_name, sum(bill_item_amount) FROM bill_item INNER JOIN bill ON bill.bill_id=bill_item.bill_id WHERE bill.bill_date LIKE ? GROUP BY bill_product_name ORDER BY sum(bill_item_amount) desc";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, date);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                BestSell bestSell = BestSell.fromRS(rs);
                list.add(bestSell);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public List<Bill> getAll(String where, String order) {
        ArrayList<Bill> list = new ArrayList();
        String sql = "SELECT * FROM customer where " + where + " ORDER BY " + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Bill customer = Bill.fromRS(rs);
                list.add(customer);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;

    }

    @Override
    public Bill save(Bill obj) {
        String sql = "INSERT INTO bill (bill_date,bill_time,bill_total,bill_discount,bill_cash,emp_id,cus_id)"
                + "VALUES(? ,? ,? ,?,?,?,?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getDate());
            stmt.setString(2, obj.getTime());
            stmt.setDouble(3, obj.getTotal());
            stmt.setDouble(4, obj.getDiscount());
            stmt.setDouble(5, obj.getCash());
            stmt.setDouble(6, obj.getEmpID());
            stmt.setDouble(7, obj.getCusID());
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public Bill update(Bill obj) {
//        String sql = "UPDATE customer"
//                + " SET cus_firstname = ?, cus_lastname = ?, cus_tel = ?, cus_score = ?"
//                + " WHERE cus_id = ?";
//        Connection conn = DatabaseHelper.getConnect();
//        try {
//            PreparedStatement stmt = conn.prepareStatement(sql);
//            stmt.setString(1, obj.getFirstname());
//            stmt.setString(2, obj.getLastname());
//            stmt.setString(3, obj.getTel());
//            stmt.setInt(4, obj.getScore());
//            stmt.setInt(5, obj.getId());
////            System.out.println(stmt);
//            int ret = stmt.executeUpdate();
//            System.out.println(ret);
//            return obj;
//        } catch (SQLException ex) {
//            System.out.println(ex.getMessage());
//            return null;
//        }
        return null;
    }

    @Override
    public int delete(Bill obj) {
        String sql = "DELETE FROM customer WHERE cus_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

    public List<Bill> getByName(String name) {
        ArrayList<Bill> list = new ArrayList();
        String sql = "SELECT * FROM customer WHERE cus_firstname LIKE ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, "%" + name + "%");
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                Bill customer = Bill.fromRS(rs);
                list.add(customer);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

}
