/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Dao;

import DatabaseHelper.DatabaseHelper;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import model.Ingredients;

/**
 *
 * @author Rattanalak
 */
public class IngredientsDao implements Dao<Ingredients> {

    @Override
    public Ingredients get(int id) {
        Ingredients ingredient = null;
        String sql = "SELECT * FROM ingredients WHERE in_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                ingredient = ingredient.fromRS(rs);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return ingredient;
    }

    public List<Ingredients> getByName(String name) {
        ArrayList<Ingredients> list = new ArrayList();
        String sql = "SELECT * FROM ingredients WHERE in_ProductName LIKE ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, "%" + name + "%");
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                Ingredients ingredient = Ingredients.fromRS(rs);
                list.add(ingredient);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public List<Ingredients> getAll() {
        ArrayList<Ingredients> list = new ArrayList();
        String sql = "SELECT * FROM ingredients";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Ingredients ingredient = Ingredients.fromRS(rs);
                list.add(ingredient);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<Ingredients> getAll(String order) {
        ArrayList<Ingredients> list = new ArrayList();
        String sql = "SELECT * FROM ingredients ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Ingredients ingredient = Ingredients.fromRS(rs);
                list.add(ingredient);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public Ingredients save(Ingredients obj) {
        String sql = "INSERT INTO ingredients (in_ProductName, in_unit, in_unitBuy, in_minimum, in_remain, in_date, emp_id)"
                + "VALUES(? ,? ,?, ?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getName());
            stmt.setString(2, obj.getUnit());
            stmt.setInt(3, obj.getUnitBuy());
            stmt.setInt(4, obj.getMinimum());
            stmt.setInt(5, obj.getRemain());
            stmt.setString(6, obj.getDate());
            stmt.setInt(7, obj.getEmployee());

//            System.out.println(stmt);
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public Ingredients update(Ingredients obj) {
        String sql = "UPDATE ingredients"
                + " SET in_ProductName = ?, in_unit = ?, in_unitBuy = ?, in_minimum=?, in_remain=?, in_date=?, emp_id= ?"
                + " WHERE in_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getName());
            stmt.setString(2, obj.getUnit());
            stmt.setInt(3, obj.getUnitBuy());
            stmt.setInt(4, obj.getMinimum());
            stmt.setInt(5, obj.getRemain());
            stmt.setString(6, obj.getDate());
            stmt.setInt(7, obj.getEmployee());
            stmt.setInt(8, obj.getId());
//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(Ingredients obj) {
        String sql = "DELETE FROM  ingredients  WHERE in_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

    @Override
    public List<Ingredients> getAll(String where, String order) {
        ArrayList<Ingredients> list = new ArrayList();
        String sql = "SELECT * FROM ingredients where " + where + " ORDER BY " + order;
        System.out.println(sql);
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Ingredients ingredient = Ingredients.fromRS(rs);
                list.add(ingredient);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;

    }

}
