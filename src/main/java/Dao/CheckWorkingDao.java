/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Dao;

import DatabaseHelper.DatabaseHelper;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import model.CheckWorking;

import model.WorkEMP;

/**
 *
 * @author User
 */
public class CheckWorkingDao implements Dao<CheckWorking>{

    @Override
    public CheckWorking get(int id) {
        CheckWorking item = null;
        String sql = "SELECT * FROM working_hours WHERE wh_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                item = CheckWorking.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return item;

    }
    
    public List<WorkEMP>getByID(int id) {
        ArrayList<WorkEMP> list = new ArrayList();
        String sql = "SELECT * FROM working_hours INNER JOIN employee ON working_hours.emp_id = employee.emp_id WHERE employee.emp_id LIKE ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, "%" + id + "%");
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                WorkEMP workEMP = WorkEMP.fromRS(rs);

                list.add(workEMP);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    public List<WorkEMP>getByFLName(String fname , String lname) {
        ArrayList<WorkEMP> list = new ArrayList();
        String sql = "SELECT * FROM working_hours INNER JOIN employee ON working_hours.emp_id = employee.emp_id WHERE employee.emp_FirstName LIKE ? and employee.emp_LastName LIKE ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, "%" + fname + "%");
            stmt.setString(2, "%" + lname + "%");
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                WorkEMP workEMP = WorkEMP.fromRS(rs);

                list.add(workEMP);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public List<CheckWorking> getAll() {
        ArrayList<CheckWorking> list = new ArrayList();
        String sql = "SELECT * FROM working_hours";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                CheckWorking working = CheckWorking.fromRS(rs);

                list.add(working);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;

    }
    
    public List<WorkEMP> getAllEMP() {
        ArrayList<WorkEMP> list = new ArrayList();
        String sql = "SELECT *  FROM working_hours INNER JOIN employee ON working_hours.emp_id = employee.emp_id";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                WorkEMP workEMP = WorkEMP.fromRS(rs);

                list.add(workEMP);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;

    }
    
    public List<WorkEMP> getAllByDay(String date) {
        ArrayList<WorkEMP> list = new ArrayList();
        String sql = "SELECT * FROM working_hours INNER JOIN employee ON working_hours.emp_id = employee.emp_id WHERE wh_date = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, date);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                WorkEMP workEMP = WorkEMP.fromRS(rs);

                list.add(workEMP);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    public List<WorkEMP> getAllByNameDate(String date, int id) {
        ArrayList<WorkEMP> list = new ArrayList();
        String sql = "SELECT *  FROM working_hours INNER JOIN employee ON working_hours.emp_id = employee.emp_id WHERE wh_date LIKE ? AND employee.emp_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, date);
            stmt.setInt(2,id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                WorkEMP workEMP = WorkEMP.fromRS(rs);

                list.add(workEMP);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    public double getAllBySumSalary(String date, int id) {

        String sql = "SELECT sum(wh_total) as SumTotal FROM working_hours INNER JOIN employee ON working_hours.emp_id = employee.emp_id WHERE wh_date LIKE ? AND employee.emp_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, date);
            stmt.setInt(2, id);
            ResultSet rs = stmt.executeQuery();
            return rs.getDouble("SumTotal");
            
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return 0.0;
    }
    
    
    @Override
    public List<CheckWorking> getAll(String where, String order) {
                ArrayList<CheckWorking> list = new ArrayList();
        String sql = "SELECT * FROM working_hours where " + where + " ORDER BY " + order;
         System.out.println(sql);
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
               CheckWorking working = CheckWorking.fromRS(rs);

                list.add(working);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;

    }

    @Override
    public CheckWorking save(CheckWorking obj) {
    String sql = "INSERT INTO working_hours (wh_date, wh_startime, wh_endtime, wh_total, emp_id )"
                + "VALUES(?,?,?,?,?)";
        Connection conn = DatabaseHelper.getConnect();
         try {
            PreparedStatement stmt = conn.prepareStatement(sql);
             System.out.println(obj);
            stmt.setString(1, obj.getDate());
            stmt.setString(2, obj.getStartTime());
            stmt.setString(3, obj.getEndTime());
            stmt.setDouble(4, obj.getTotal());
            stmt.setInt(5, obj.getEmpid());
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public CheckWorking update(CheckWorking obj) {
        String sql = "UPDATE working_hours"
                + " SET wh_endtime = ?, wh_total = ?"
                + " WHERE wh_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getEndTime());
            stmt.setDouble(2, obj.getTotal());
            stmt.setInt(3, obj.getId());
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            return get(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(CheckWorking obj) {
//        String sql = "DELETE FROM user WHERE wh_id=?";
//        Connection conn = DatabaseHelper.getConnect();
//        try {
//            PreparedStatement stmt = conn.prepareStatement(sql);
//            stmt.setInt(1, obj.getId());
//            int ret = stmt.executeUpdate();
//            return ret;
//        } catch (SQLException ex) {
//            System.out.println(ex.getMessage());
//        }
        return -1;
    }
    
}
