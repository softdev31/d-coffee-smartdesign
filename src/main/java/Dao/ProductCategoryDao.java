/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Dao;

import DatabaseHelper.DatabaseHelper;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import model.ProductCategory;

/**
 *
 * @author 66986
 */
public class ProductCategoryDao implements Dao<ProductCategory>{

    @Override
    public ProductCategory get(int id) {
          ProductCategory productcategory = null;
        String sql = "SELECT * FROM product_Category WHERE pc_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                productcategory = ProductCategory.fromRS(rs);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return productcategory;
    
    }

    @Override
    public List<ProductCategory> getAll() {
        ArrayList<ProductCategory> list = new ArrayList();
        String sql = "SELECT * FROM product_Category";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                ProductCategory  productcategory = ProductCategory.fromRS(rs);

                list.add(productcategory);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    
    }

    @Override
    public ProductCategory save(ProductCategory obj) {
         String sql = "INSERT INTO product_Category (pc_name)"
                + "VALUES (?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getName());

            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public ProductCategory update(ProductCategory obj) {
        String sql = "UPDATE product_Category"
                + " SET pc_name = ?"
                + " WHERE pc_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getName());
            stmt.setInt(2, obj.getId());
            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(ProductCategory obj) {
       String sql = "DELETE FROM product_Category WHERE pc_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

    @Override
    public List<ProductCategory> getAll(String where, String order) {
       ArrayList<ProductCategory> list = new ArrayList();
        String sql = "SELECT * FROM product_Category where " + where + " ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                ProductCategory item = ProductCategory.fromRS(rs);
                list.add(item);

            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
}
