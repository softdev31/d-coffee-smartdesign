/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Dao;

import DatabaseHelper.DatabaseHelper;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import model.Bill;
import model.BillItem;

/**
 *
 * @author ASUS
 */
public class BillItemDao implements Dao<BillItem> {

    @Override
    public BillItem get(int id) {
        BillItem billItem = null;
        String sql = "SELECT * FROM check_stock WHERE cs_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                billItem = billItem.fromRS(rs);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return billItem;
    }

    @Override
    public List<BillItem> getAll() {
        ArrayList<BillItem> list = new ArrayList();
        String sql = "SELECT * FROM check_stock";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                BillItem billItem = BillItem.fromRS(rs);

                list.add(billItem);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<BillItem> getAll(String order) {
        ArrayList<BillItem> list = new ArrayList();
        String sql = "SELECT * FROM check_stock ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                BillItem item = BillItem.fromRS(rs);

                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    public List<BillItem> getAllByBillID(int id) {
        ArrayList<BillItem> list = new ArrayList();
        String sql = "SELECT * FROM bill_item WHERE bill_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                BillItem item = BillItem.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public BillItem save(BillItem obj) {
        String sql = "INSERT INTO bill_item (bill_product_name,bill_item_amount,bill_item_size,bill_item_sweet,bill_product_price,pro_id,bill_id)"
                + "VALUES(? ,? ,? ,?,?,?,?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getName());
            stmt.setInt(2, obj.getAmount());
            stmt.setString(3, obj.getSize());
            stmt.setInt(4, obj.getSweet());
            stmt.setDouble(5, obj.getPrice());
            stmt.setInt(6, obj.getProId());
            stmt.setInt(7, obj.getBillid());
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public BillItem update(BillItem obj) {
        return null;
    }

    @Override
    public int delete(BillItem obj) {
        String sql = "DELETE FROM check_stock WHERE cs_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

    @Override
    public List<BillItem> getAll(String where, String order) {
        ArrayList<BillItem> list = new ArrayList();
        String sql = "SELECT * FROM check_stock where " + where + " ORDER BY " + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                BillItem item = BillItem.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
}
