/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Dao;

import DatabaseHelper.DatabaseHelper;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import model.Product;


/**
 *
 * @author 66986
 */
public class ProductDao implements Dao <Product> {

    @Override
    public Product get(int id) {
       Product item = null;
        String sql = "SELECT * FROM product WHERE pro_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                item = Product.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return item;
    }
    //test
    public List<Product> getAll(String order) {
        ArrayList<Product> list = new ArrayList();
        String sql = "SELECT * FROM product  ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Product item = Product.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }



    @Override
    public ArrayList<Product> getAll() {
         ArrayList<Product> list = new ArrayList();
        String sql = "SELECT * FROM product";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Product item = Product.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    public ArrayList<Product> getType(int type) {
        ArrayList<Product> list = new ArrayList();
        String sql = "SELECT * FROM product WHERE pc_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, type);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                Product item = Product.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }


    @Override
    public Product save(Product obj) {
         String sql = "INSERT INTO product (pro_name, pro_price, pc_id, pro_image)"
                + "VALUES(? ,?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getName());
            stmt.setDouble(2, obj.getPrice());
            stmt.setInt(3, obj.getProduct_Category());
            stmt.setString(4, obj.getImage());

            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public Product update(Product obj) {
       String sql = "UPDATE product"
                + " SET pro_name = ?, pro_price = ?, pc_id = ?, pro_image = ?"
                + " WHERE pro_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getName());
            stmt.setDouble(2, obj.getPrice());
            stmt.setInt(3, obj.getProduct_Category());
            stmt.setString(4, obj.getImage());
            stmt.setInt(5, obj.getId());
            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(Product obj) {
       String sql = "DELETE FROM product WHERE pro_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    
    }

    @Override
    public List<Product> getAll(String where, String order) {
       ArrayList<Product> list = new ArrayList();
        String sql = "SELECT * FROM product where " + where + " ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Product item = Product.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<Product> getByname(String name) {
       
        ArrayList<Product> list = new ArrayList();
        String sql = "SELECT * FROM product WHERE pro_name LIKE ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, "%" + name + "%");
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                Product productTwo = Product.fromRS(rs);
                list.add(productTwo);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

       
    
    
}
