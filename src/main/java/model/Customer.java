/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author OS
 */
public class Customer {

    private int id;
    private String firstname;
    private String lastname;
    private String tel;
    private int score;

    public Customer() {
        this.id = -1;
    }

    @Override
    public String toString() {
        return "Customer{" + "id=" + id + ", firstname=" + firstname + ", lastname=" + lastname + ", tel=" + tel + ", score=" + score + '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public Customer(String firstname, String lastname, String tel, int score) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.tel = tel;
        this.score = score;
    }

    public Customer(int id, String firstname, String lastname, String tel, int score) {
        this.id = id;
        this.firstname = firstname;
        this.lastname = lastname;
        this.tel = tel;
        this.score = score;
    }

    public static Customer fromRS(ResultSet rs) {
        Customer customer = new Customer();
        try {
            customer.setId(rs.getInt(("cus_id")));
            customer.setFirstname(rs.getString(("cus_firstname")));
            customer.setLastname(rs.getString(("cus_lastname")));
            customer.setTel(rs.getString(("cus_tel")));
            customer.setScore(rs.getInt(("cus_score")));

        } catch (SQLException ex) {
            Logger.getLogger(Customer.class.getName()).log(Level.SEVERE, null, ex);

        }
        return customer;
    }
}
