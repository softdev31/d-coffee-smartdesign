/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author 66986
 */
public class Product {
    private int id;
    private String name;
    private double price;
    private int product_Category;
    private String image;

    public Product(int id, String name, double price, int product_Category, String image) {
        this.id = id;
        this.name = name;
      this.price = price;
        this.product_Category = product_Category;
        this.image = image;
    }
    /*
     public Product(String name,double price, int product_Category , String image) {
        this.id = id;
        this.name = name;     
       this.price = price;
        this.product_Category = product_Category;
        this.image = image;
        
    
    }*/
     
    public Product() {
        this.id = -1;
        this.product_Category=0;
        this.name="";
        this.image="";
        this.price=0;
        
    
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getProduct_Category() {
        return product_Category;
    }

    public void setProduct_Category(int product_Category) {
        this.product_Category = product_Category;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
    
    public static Product fromRS(ResultSet rs){
        Product product = new Product();
        
            try {
                product.setId(rs.getInt("pro_id"));
                product.setName(rs.getString("pro_name"));
                product.setPrice(rs.getDouble("pro_price"));
                product.setProduct_Category(rs.getInt("pc_id"));
                product.setImage(rs.getString("pro_image"));
        
            } catch (SQLException ex) {
                Logger.getLogger(Product.class.getName()).log(Level.SEVERE, null, ex);
            }
            return product;
            
        
    }
    

    @Override
    public String toString() {
        return "Product{" + "id=" + id + ", name=" + name + ", price=" + price + ", product_Category=" + product_Category + ", image=" + image + '}';
    }
}

    
     
     
     
    
    
    
    
    
    
    
    

