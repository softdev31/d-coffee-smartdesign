/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ASUS
 */
public class WorkEMP {
    private int id;
    private String date;
    String starttime;
    String endtime;
    Double totel;
    int emp_id;
    String emp_Fname;
    String emp_Lname;

    public WorkEMP(int id, String date, String starttime, String endtime, Double totel, int emp_id, String emp_Fname, String emp_Lname) {
        this.id = id;
        this.date = date;
        this.starttime = starttime;
        this.endtime = endtime;
        this.totel = totel;
        this.emp_id = emp_id;
        this.emp_Fname = emp_Fname;
        this.emp_Lname = emp_Lname;
    }

    public WorkEMP() {
        this.id = -1;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getStarttime() {
        return starttime;
    }

    public void setStarttime(String starttime) {
        this.starttime = starttime;
    }

    public String getEndtime() {
        return endtime;
    }

    public void setEndtime(String endtime) {
        this.endtime = endtime;
    }

    public Double getTotel() {
        return totel;
    }

    public void setTotel(Double totel) {
        this.totel = totel;
    }

    public int getEmp_id() {
        return emp_id;
    }

    public void setEmp_id(int emp_id) {
        this.emp_id = emp_id;
    }

    public String getEmp_Fname() {
        return emp_Fname;
    }

    public void setEmp_Fname(String emp_Fname) {
        this.emp_Fname = emp_Fname;
    }

    public String getEmp_Lname() {
        return emp_Lname;
    }

    public void setEmp_Lname(String emp_Lname) {
        this.emp_Lname = emp_Lname;
    }

    @Override
    public String toString() {
        return "WorkEMP{" + "id=" + id + ", date=" + date + ", starttime=" + starttime + ", endtime=" + endtime + ", totel=" + totel + ", emp_id=" + emp_id + ", emp_Fname=" + emp_Fname + ", emp_Lname=" + emp_Lname + '}';
    }
    
    public static WorkEMP fromRS(ResultSet rs) {
        WorkEMP workEMP = new WorkEMP();
        try {
            workEMP.setId(rs.getInt("wh_id"));
            workEMP.setDate(rs.getString("wh_date"));
            workEMP.setStarttime(rs.getString("wh_startime"));
            workEMP.setEndtime(rs.getString("wh_endtime"));
            workEMP.setTotel(rs.getDouble("wh_total"));
            workEMP.setEmp_id(rs.getInt("emp_id"));
            workEMP.setEmp_Fname(rs.getString("emp_FirstName"));
            workEMP.setEmp_Lname(rs.getString("emp_LastName"));
            
        } catch (SQLException ex) {
            Logger.getLogger(WorkEMP.class.getName()).log(Level.SEVERE, null, ex);
        }
        return workEMP;
    }
    
    
}
