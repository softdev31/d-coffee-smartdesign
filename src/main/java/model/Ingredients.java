/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import Dao.EmployeeDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Rattanalak
 */
public class Ingredients {

    private int id;
    private String name;
    private String unit;
    private int unitBuy;
    private int minimum;
    private int remain;
    private String date;
    private int employee;
    private int increase;

    public Ingredients(int id, String name, String unit, int unitBuy, int minimum, int remain, String date, int employee, int increase) {
        this.id = id;
        this.name = name;
        this.unit = unit;
        this.unitBuy = unitBuy;
        this.minimum = minimum;
        this.remain = remain;
        this.date = date;
        this.employee = employee;
        this.increase = increase;
    }

    public Ingredients(String name, String unit, int unitBuy, int minimum, int remain, String date, int employee,int increase) {
        this.id = -1;
        this.name = name;
        this.unit = unit;
        this.unitBuy = unitBuy;
        this.minimum = minimum;
        this.remain = remain;
        this.date = date;
        this.employee = employee;
        this.increase = increase;
    }

    public Ingredients() {
        this.id = -1;
        this.name = "";
        this.unit = "";
        this.unitBuy = 0;
        this.minimum = 0;
        this.remain = 0 ;
        this.date = "";
        this.employee = 0;
        this.increase = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public int getUnitBuy() {
        return unitBuy;
    }

    public void setUnitBuy(int unitBuy) {
        this.unitBuy = unitBuy;
    }

    public int getMinimum() {
        return minimum;
    }

    public void setMinimum(int minimum) {
        this.minimum = minimum;
    }

    public int getRemain() {
        return remain;
    }

    public void setRemain(int remain) {
        this.remain = remain;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getEmployee() {
        return employee;
    }

    public void setEmployee(int employee) {
        this.employee = employee;
    }

    public int getIncrease() {
        return increase;
    }

    public void setIncrease(int increase) {
        this.increase = increase;
    }

    @Override
    public String toString() {
        return "Ingredients{" + "id=" + id + ", name=" + name + ", unit=" + unit + ", unitBuy=" + unitBuy + ", minimum=" + minimum + ", remain=" + remain + ", date=" + date + ", employee=" + employee + ", increase=" + increase + '}';
    }
    

   

    public static Ingredients fromRS(ResultSet rs) {
        Ingredients ingredients = new Ingredients();
        Employee emp = new Employee();
         EmployeeDao empDao = new EmployeeDao();
        try {
            ingredients.setId(rs.getInt("in_id"));
            ingredients.setName(rs.getString("in_ProductName"));
            ingredients.setUnit(rs.getString("in_unit"));
            ingredients.setUnitBuy(rs.getInt("in_unitBuy"));
            ingredients.setMinimum(rs.getInt("in_minimum"));
            ingredients.setRemain(rs.getInt("in_remain"));
            ingredients.setDate(rs.getString("in_date"));
            ingredients.setEmployee(rs.getInt("emp_id"));
             ingredients.setIncrease(rs.getInt("in_increase"));

        } catch (SQLException ex) {
            Logger.getLogger(Ingredients.class.getName()).log(Level.SEVERE, null, ex);
        }
        return ingredients;
    }

}
