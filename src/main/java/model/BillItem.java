/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ASUS
 */
public class BillItem {
    private int id;
    private String name;
    private int amount;
    private String size;
    private int sweet;
    private double price;
    private int proId;
    private int Billid;

    public BillItem(int id, String name, int amount, String size, int sweet, double price, int proId, int Billid) {
        this.id = id;
        this.name = name;
        this.amount = amount;
        this.size = size;
        this.sweet = sweet;
        this.price = price;
        this.proId = proId;
        this.Billid = Billid;
    }

    public BillItem() {
        this.id = -1;
        this.name = "";
        this.amount = 0;
        this.size = "";
        this.sweet = 0;
        this.price = 0;
        this.proId = 0;
        this.Billid = 0;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public int getSweet() {
        return sweet;
    }

    public void setSweet(int sweet) {
        this.sweet = sweet;
    }
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getProId() {
        return proId;
    }

    public void setProId(int proId) {
        this.proId = proId;
    }

    public int getBillid() {
        return Billid;
    }

    public void setBillid(int Billid) {
        this.Billid = Billid;
    }

    @Override
    public String toString() {
        return "BillItem{" + "id=" + id + ", name=" + name + ", amount=" + amount + ", size=" + size + ", sweet=" + sweet + ", price=" + price + ", proId=" + proId + ", Billid=" + Billid + '}';
    }


    
    public static BillItem fromRS(ResultSet rs) {
       BillItem billItem = new BillItem();
        try {
            billItem.setId(rs.getInt("bill_item_id"));
            billItem.setName(rs.getString("bill_product_name"));
            billItem.setAmount(rs.getInt("bill_item_amount"));
            billItem.setSize(rs.getString("bill_item_size"));
            billItem.setSweet(rs.getInt("bill_item_sweet"));
            billItem.setPrice(rs.getDouble("bill_product_price"));
            billItem.setProId(rs.getInt("pro_id"));
            billItem.setBillid(rs.getInt("bill_id"));    
        } catch (SQLException ex) {
            Logger.getLogger(Bill.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return billItem;
    }
    
}
