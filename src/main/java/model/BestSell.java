/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ASUS
 */
public class BestSell {
    private String name;
    private int sumamount;

    public BestSell(String name, int sumamount) {
        this.name = name;
        this.sumamount = sumamount;
    }

    public BestSell() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSumamount() {
        return sumamount;
    }

    public void setSumamount(int sumamount) {
        this.sumamount = sumamount;
    }

    @Override
    public String toString() {
        return "bestSell{" + "name=" + name + ", sumamount=" + sumamount + '}';
    }
    
    public static BestSell fromRS(ResultSet rs) {
       BestSell bestSell = new BestSell();
        try {
            bestSell.setName(rs.getString("bill_product_name"));
            bestSell.setSumamount(rs.getInt("sum(bill_item_amount)"));
        } catch (SQLException ex) {
            Logger.getLogger(BestSell.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return bestSell;
    }
    
}
