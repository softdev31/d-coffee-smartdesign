/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ASUS
 */
public class Bill {
    private int id;
    private String date;
    private String time;
    private double total;
    private double discount;
    private double cash;
    private int empID;
    private int cusID;

    public Bill(int id, String date, String time, double total, double discount, double cash, int empID, int cusID) {
        this.id = id;
        this.date = date;
        this.time = time;
        this.total = total;
        this.discount = discount;
        this.cash = cash;
        this.empID = empID;
        this.cusID = cusID;
    }

    public Bill() {
        this.id = -1;
        this.date = "00/00/0000";
        this.time = "00:00:00";
        this.total = 0;
        this.discount = 0;
        this.cash = 0;
        this.empID = -1;
        this.cusID = -1;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    public double getCash() {
        return cash;
    }

    public void setCash(double cash) {
        this.cash = cash;
    }

    public int getEmpID() {
        return empID;
    }

    public void setEmpID(int empID) {
        this.empID = empID;
    }

    public int getCusID() {
        return cusID;
    }

    public void setCusID(int cusID) {
        this.cusID = cusID;
    }

    @Override
    public String toString() {
        return "Bill{" + "id=" + id + ", date=" + date + ", time=" + time + ", total=" + total + ", discount=" + discount + ", cash=" + cash + ", empID=" + empID + ", cusID=" + cusID + '}';
    }
    
    public static Bill fromRS(ResultSet rs) {
       Bill bill = new Bill();
        try {
            bill.setId(rs.getInt("bill_id"));
            bill.setDate(rs.getString("bill_date"));
            bill.setTime(rs.getString("bill_time"));
            bill.setTotal(rs.getDouble("bill_total"));
            bill.setDiscount(rs.getDouble("bill_discount"));
            bill.setCash(rs.getDouble("bill_cash"));
            bill.setEmpID(rs.getInt("emp_id"));
            bill.setCusID(rs.getInt("cus_id"));
            
        } catch (SQLException ex) {
            Logger.getLogger(Bill.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return bill;
    }
        
}
