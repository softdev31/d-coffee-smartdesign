/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import Dao.EmployeeDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author User
 */
public class CheckWorking {
        private int id;
        private String date;
        private String startTime;
        private String endTime;
        private double total;
        private int empid;

    public CheckWorking(int id, String date, String startTime, String endTime, double total, int empid) {
        this.id = id;
        this.date = date;
        this.startTime = startTime;
        this.endTime = endTime;
        this.total = total;
        this.empid = empid;
    }
    

    public CheckWorking() {
        this.id = -1;
        this.date = "";
        this.startTime = "";
        this.endTime = "";
        this.total = 0;
        this.empid = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public int getEmpid() {
        return empid;
    }

    public void setEmpid(int empid) {
        this.empid = empid;
    }

    @Override
    public String toString() {
        return "CheckWorking{" + "id=" + id + ", date=" + date + ", startTime=" + startTime + ", endTime=" + endTime + ", total=" + total + ", empid=" + empid + '}';
    }
            
        
     
    public static CheckWorking fromRS(ResultSet rs) {
       CheckWorking working = new CheckWorking();
        try {
           working.setId(rs.getInt("wh_id"));
            working.setDate(rs.getString("wh_date"));
            working.setStartTime(rs.getString("wh_startime"));
            working.setEndTime(rs.getString("wh_endtime"));
            working.setTotal(rs.getDouble("wh_total"));
            working.setEmpid(rs.getInt("emp_id"));
        } catch (SQLException ex) {
            Logger.getLogger(CheckWorking.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return working;
    }
        
}
