/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author ASUS
 */
public class Employee {
    private int id;
    private String FirstName;
    private String LastName;
    private int Age;    
    private String Tel;
    private String Gender;
    private double Salary;
    //private Store 

    public Employee(int id, String FirstName, String LastName, int Age, String Tel, String Gender, double Salary) {
        this.id = id;
        this.FirstName = FirstName;
        this.LastName = LastName;
        this.Age = Age;
        this.Tel = Tel;
        this.Gender = Gender;
        this.Salary = Salary;
    }

    public Employee(){
        this.id = -1;
        this.FirstName = "";
        this.LastName ="";
        this.Age = 0;
        this.Tel = "";
        this.Gender = "M";
        this.Salary = 0;
    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String FirstName) {
        this.FirstName = FirstName;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String LastName) {
        this.LastName = LastName;
    }

    public int getAge() {
        return Age;
    }

    public void setAge(int Age) {
        this.Age = Age;
    }


    public String getTel() {
        return Tel;
    }

    public void setTel(String Tel) {
        this.Tel = Tel;
    }

    public String getGender() {
        return Gender;
    }

    public void setGender(String Gender) {
        this.Gender = Gender;
    }


    public double getSalary() {
        return Salary;
    }

    public void setSalary(double Salary) {
        this.Salary = Salary;
    }

    @Override
    public String toString() {
        return "Employee{" + "id=" + id + ", FirstName=" + FirstName + ", LastName=" + LastName + ", Age=" + Age + ", Tel=" + Tel + ", Gender=" + Gender +", Salary=" + Salary + '}';
    }
    
    public static Employee fromRS(ResultSet rs) {
       Employee employee = new Employee();
        try {
            employee.setId(rs.getInt("emp_id"));
            employee.setFirstName(rs.getString("emp_FirstName"));
            employee.setLastName(rs.getString("emp_LastName"));
            employee.setAge(rs.getInt("emp_Age"));
            employee.setTel(rs.getString("emp_tel"));
            employee.setGender(rs.getString("emp_gender"));
            employee.setSalary(rs.getDouble("emp_salary"));
        } catch (SQLException ex) {
            //Logger.getLogger(Orders.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return employee;
    }

}
