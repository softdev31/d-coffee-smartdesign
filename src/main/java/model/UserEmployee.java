/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author User
 */
public class UserEmployee {
    private int id;
    private String name = "";
    private String password = "";
    private int role;
    private String fristname = "";
    private String lastname = "";
    private Employee employee;
    public UserEmployee() {
        this.id = -1;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    
    public UserEmployee(int id, String name, String password, int role, String fristname, String lastname) {
        this.id = id;
        this.name = name;
        this.password = password;
        this.role = role;
        this.fristname = fristname;
        this.lastname = lastname;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getRole() {
        return role;
    }

    public void setRole(int role) {
        this.role = role;
    }

    public String getFristname() {
        return fristname;
    }

    public void setFristname(String fristname) {
        this.fristname = fristname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    @Override
    public String toString() {
        return "UserEmployee{" + "id=" + id + ", name=" + name + ", password=" + password + ", role=" + role + ", fristname=" + fristname + ", lastname=" + lastname + '}';
    }
    
    
    public static UserEmployee fromRS(ResultSet rs) {
        UserEmployee useremployee = new UserEmployee();
        try {
            useremployee.setId(rs.getInt("user_id"));
            useremployee.setName(rs.getString("user_name"));
            useremployee.setPassword(rs.getString("user_password"));
            useremployee.setRole(rs.getInt("user_role"));
            useremployee.setFristname(rs.getString("emp_FirstName"));
            useremployee.setLastname(rs.getString("emp_LastName"));
           int empID = rs.getInt("emp_id");
        } catch (SQLException ex) {
            Logger.getLogger(UserEmployee.class.getName()).log(Level.SEVERE, null, ex);
        }
        return useremployee;
    }
}
