/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package UserService;

import Dao.UserDao;
import model.User;

/**
 *
 * @author ASUS
 */
public class UserService {

    public static User login(String login, String pass) {
        UserDao userDao = new UserDao();
        User user = userDao.getByName(login);
        if(user != null && user.getPassword().equals(pass)){
            return user;
        }
        return null;
    }
}
