/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JPanel.java to edit this template
 */
package UiDcoffee;

import javax.swing.ImageIcon;
import model.User;

/**
 *
 * @author ASUS
 */
public class MenuPanel extends javax.swing.JPanel {

    User user;
    private MainFrame mainFrame;

    /**
     * Creates new form MenuPanel
     */
    public MenuPanel(MainFrame mainFrame, User user) {
        initComponents();
        this.user = user;
        Logo.setIcon(new ImageIcon("./images/logo.png"));
        btpos.setIcon(new ImageIcon("./Icon/1.png"));
        btStock.setIcon(new ImageIcon("./Icon/2.png"));
        btOrder.setIcon(new ImageIcon("./Icon/3.png"));
        btCustomer.setIcon(new ImageIcon("./Icon/4.png"));
        btEmployee.setIcon(new ImageIcon("./Icon/5.png"));
        btUser.setIcon(new ImageIcon("./Icon/6.png"));
        btProduct.setIcon(new ImageIcon("./Icon/menu30px.png"));
        btsalary.setIcon(new ImageIcon("./Icon/cash30px.png"));
        btReport.setIcon(new ImageIcon("./Icon/report30px.png"));
        btINOUT.setIcon(new ImageIcon("./Icon/Checkmark30px.png"));
        btlogout.setIcon(new ImageIcon("./Icon/door30px.png"));
        this.mainFrame = mainFrame;
        txtName.setText(user.getName());
        if (user.getRole() == 1) {
            txtrole.setText("เจ้าของร้าน");
        } else {
            txtrole.setText("พนักงาน");
            //btOrder.setVisible(false);
            btCustomer.setVisible(false);
            btEmployee.setVisible(false);
            btUser.setVisible(false);
            btProduct.setVisible(false);
            btsalary.setVisible(false);
            btReport.setVisible(false);
        }
        srcMain.setViewportView(new PosPanel(user));
        iconBer.setIcon(new ImageIcon("./Icon/Pos80px.png"));
        txtNameBar.setText("ขายสินค้า");

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        MenuP = new javax.swing.JPanel();
        btpos = new javax.swing.JButton();
        btOrder = new javax.swing.JButton();
        btStock = new javax.swing.JButton();
        btCustomer = new javax.swing.JButton();
        btlogout = new javax.swing.JButton();
        btEmployee = new javax.swing.JButton();
        btUser = new javax.swing.JButton();
        Logo = new javax.swing.JLabel();
        btProduct = new javax.swing.JButton();
        btsalary = new javax.swing.JButton();
        btReport = new javax.swing.JButton();
        btINOUT = new javax.swing.JButton();
        srcMain = new javax.swing.JScrollPane();
        BarPanel = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txtName = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        txtrole = new javax.swing.JLabel();
        iconBer = new javax.swing.JLabel();
        txtNameBar = new javax.swing.JLabel();

        setBackground(new java.awt.Color(255, 255, 255));
        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        MenuP.setBackground(new java.awt.Color(188, 158, 134));
        MenuP.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0), 2));

        btpos.setBackground(new java.awt.Color(205, 185, 174));
        btpos.setFont(new java.awt.Font("MN CUSTARD Light", 0, 18)); // NOI18N
        btpos.setText("ขายสินค้า");
        btpos.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btpos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btposActionPerformed(evt);
            }
        });

        btOrder.setBackground(new java.awt.Color(205, 185, 174));
        btOrder.setFont(new java.awt.Font("MN CUSTARD Light", 0, 18)); // NOI18N
        btOrder.setText("รายการคำสังซื้อ");
        btOrder.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btOrder.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btOrderActionPerformed(evt);
            }
        });

        btStock.setBackground(new java.awt.Color(205, 185, 174));
        btStock.setFont(new java.awt.Font("MN CUSTARD Light", 0, 18)); // NOI18N
        btStock.setText("สต๊อกสินค้า");
        btStock.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btStock.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btStockActionPerformed(evt);
            }
        });

        btCustomer.setBackground(new java.awt.Color(205, 185, 174));
        btCustomer.setFont(new java.awt.Font("MN CUSTARD Light", 0, 18)); // NOI18N
        btCustomer.setText("ลูกค้า");
        btCustomer.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btCustomer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btCustomerActionPerformed(evt);
            }
        });

        btlogout.setBackground(new java.awt.Color(255, 102, 102));
        btlogout.setFont(new java.awt.Font("MN CUSTARD Light", 0, 18)); // NOI18N
        btlogout.setText("ออกจากระบบ");
        btlogout.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btlogout.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btlogoutActionPerformed(evt);
            }
        });

        btEmployee.setBackground(new java.awt.Color(205, 185, 174));
        btEmployee.setFont(new java.awt.Font("MN CUSTARD Light", 0, 18)); // NOI18N
        btEmployee.setText("พนักงาน");
        btEmployee.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btEmployee.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btEmployeeActionPerformed(evt);
            }
        });

        btUser.setBackground(new java.awt.Color(205, 185, 174));
        btUser.setFont(new java.awt.Font("MN CUSTARD Light", 0, 18)); // NOI18N
        btUser.setText("ผู้ใช้งาน");
        btUser.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btUser.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btUserActionPerformed(evt);
            }
        });

        Logo.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

        btProduct.setBackground(new java.awt.Color(205, 185, 174));
        btProduct.setFont(new java.awt.Font("MN CUSTARD Light", 0, 18)); // NOI18N
        btProduct.setText("รายการสินค้า");
        btProduct.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btProduct.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btProductActionPerformed(evt);
            }
        });

        btsalary.setBackground(new java.awt.Color(205, 185, 174));
        btsalary.setFont(new java.awt.Font("MN CUSTARD Light", 0, 18)); // NOI18N
        btsalary.setText("จ่ายเงินเดือน");
        btsalary.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btsalary.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btsalaryActionPerformed(evt);
            }
        });

        btReport.setBackground(new java.awt.Color(205, 185, 174));
        btReport.setFont(new java.awt.Font("MN CUSTARD Light", 0, 18)); // NOI18N
        btReport.setText("Report");
        btReport.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btReport.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btReportActionPerformed(evt);
            }
        });

        btINOUT.setBackground(new java.awt.Color(205, 185, 174));
        btINOUT.setFont(new java.awt.Font("MN CUSTARD Light", 0, 18)); // NOI18N
        btINOUT.setText("เช็ค อิน-เอาท์");
        btINOUT.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btINOUT.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btINOUTActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout MenuPLayout = new javax.swing.GroupLayout(MenuP);
        MenuP.setLayout(MenuPLayout);
        MenuPLayout.setHorizontalGroup(
            MenuPLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(MenuPLayout.createSequentialGroup()
                .addGap(28, 28, 28)
                .addGroup(MenuPLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(MenuPLayout.createSequentialGroup()
                        .addGroup(MenuPLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(btOrder, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(Logo, javax.swing.GroupLayout.PREFERRED_SIZE, 162, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btlogout, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addContainerGap(31, Short.MAX_VALUE))
                    .addGroup(MenuPLayout.createSequentialGroup()
                        .addGroup(MenuPLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(btStock, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btINOUT, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btReport, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btsalary, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btProduct, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btUser, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btEmployee, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btCustomer, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(MenuPLayout.createSequentialGroup()
                                .addGap(2, 2, 2)
                                .addComponent(btpos, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 0, Short.MAX_VALUE))))
        );
        MenuPLayout.setVerticalGroup(
            MenuPLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, MenuPLayout.createSequentialGroup()
                .addGap(47, 47, 47)
                .addComponent(Logo, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(87, 87, 87)
                .addComponent(btpos, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btOrder, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(12, 12, 12)
                .addComponent(btStock, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btEmployee, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btUser, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(12, 12, 12)
                .addComponent(btCustomer, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btProduct, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btsalary, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btReport, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 144, Short.MAX_VALUE)
                .addComponent(btINOUT, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btlogout, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(29, 29, 29))
        );

        add(MenuP, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, -1, 1013));

        srcMain.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0), 2));
        add(srcMain, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 104, 1693, 909));

        BarPanel.setBackground(new java.awt.Color(176, 141, 111));
        BarPanel.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0), 2));

        jLabel1.setFont(new java.awt.Font("MN CUSTARD Light", 0, 24)); // NOI18N
        jLabel1.setText("ผู้ใช้ระบบ : ");

        txtName.setFont(new java.awt.Font("MN CUSTARD Light", 0, 24)); // NOI18N
        txtName.setText("xxxxxxxxxxxx");

        jLabel3.setFont(new java.awt.Font("MN CUSTARD Light", 0, 24)); // NOI18N
        jLabel3.setText("สถานะ : ");

        txtrole.setFont(new java.awt.Font("MN CUSTARD Light", 0, 24)); // NOI18N
        txtrole.setText("xxxxxxxxxxx");

        iconBer.setText("jLabel2");

        txtNameBar.setFont(new java.awt.Font("MN CUSTARD Light", 0, 48)); // NOI18N
        txtNameBar.setText("jLabel2");

        javax.swing.GroupLayout BarPanelLayout = new javax.swing.GroupLayout(BarPanel);
        BarPanel.setLayout(BarPanelLayout);
        BarPanelLayout.setHorizontalGroup(
            BarPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, BarPanelLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(iconBer, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(27, 27, 27)
                .addComponent(txtNameBar, javax.swing.GroupLayout.PREFERRED_SIZE, 371, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(958, 958, 958)
                .addGroup(BarPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel1)
                    .addComponent(jLabel3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(BarPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtName)
                    .addComponent(txtrole))
                .addGap(35, 35, 35))
        );
        BarPanelLayout.setVerticalGroup(
            BarPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(BarPanelLayout.createSequentialGroup()
                .addGap(14, 14, 14)
                .addGroup(BarPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(BarPanelLayout.createSequentialGroup()
                        .addGroup(BarPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel1)
                            .addComponent(txtName))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(BarPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel3)
                            .addComponent(txtrole)))
                    .addGroup(BarPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(txtNameBar)
                        .addComponent(iconBer, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(7, Short.MAX_VALUE))
        );

        add(BarPanel, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 0, 1693, 105));
    }// </editor-fold>//GEN-END:initComponents

    private void btposActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btposActionPerformed
        srcMain.setViewportView(new PosPanel(user));
        iconBer.setIcon(new ImageIcon("./Icon/Pos80px.png"));
        txtNameBar.setText("ขายสินค้า");
    }//GEN-LAST:event_btposActionPerformed

    private void btOrderActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btOrderActionPerformed
        srcMain.setViewportView(new OrderPanel());
        iconBer.setIcon(new ImageIcon("./Icon/Order80px.png"));
        txtNameBar.setText("ข้อมูลรายการคำสั่งซื้อ");
    }//GEN-LAST:event_btOrderActionPerformed

    private void btStockActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btStockActionPerformed
        srcMain.setViewportView(new StockPanel(user));
        iconBer.setIcon(new ImageIcon("./Icon/Stock80px.png"));
        txtNameBar.setText("สต๊อกสินค้า");
    }//GEN-LAST:event_btStockActionPerformed

    private void btCustomerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btCustomerActionPerformed
        srcMain.setViewportView(new CustomerPanel(2));
        iconBer.setIcon(new ImageIcon("./Icon/customer80px.png"));
        txtNameBar.setText("ข้อมูลลูกค้า");
    }//GEN-LAST:event_btCustomerActionPerformed

    private void btEmployeeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btEmployeeActionPerformed
        srcMain.setViewportView(new EmployeePanel());
        iconBer.setIcon(new ImageIcon("./Icon/employee80px.png"));
        txtNameBar.setText("ข้อมูลพนักงาน");
    }//GEN-LAST:event_btEmployeeActionPerformed

    private void btUserActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btUserActionPerformed
        srcMain.setViewportView(new UserPanel());
        iconBer.setIcon(new ImageIcon("./Icon/User80px.png"));
        txtNameBar.setText("ข้อมูลผู้ใช้งาน");
    }//GEN-LAST:event_btUserActionPerformed

    private void btlogoutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btlogoutActionPerformed
        mainFrame.Logout();
    }//GEN-LAST:event_btlogoutActionPerformed

    private void btProductActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btProductActionPerformed
        srcMain.setViewportView(new ProductPanel());
        iconBer.setIcon(new ImageIcon("./Icon/Produce80px.png"));
        txtNameBar.setText("ข้อมูลรายการสินค้า");
    }//GEN-LAST:event_btProductActionPerformed

    private void btsalaryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btsalaryActionPerformed
        srcMain.setViewportView(new SalaryPanel());
        iconBer.setIcon(new ImageIcon("./Icon/money80px.png"));
        txtNameBar.setText("ข้อมูลการจ่ายเงินเดือน");
    }//GEN-LAST:event_btsalaryActionPerformed

    private void btReportActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btReportActionPerformed
        srcMain.setViewportView(new ReportPanel());
        iconBer.setIcon(new ImageIcon("./Icon/report80px.png"));
        txtNameBar.setText("ข้อมูลReport");
    }//GEN-LAST:event_btReportActionPerformed

    private void btINOUTActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btINOUTActionPerformed
        
        //iconBer.setIcon(new ImageIcon("./Icon/employee80px.png"));
        workingFrame f1 = new workingFrame();
        f1.setVisible(true);
    }//GEN-LAST:event_btINOUTActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel BarPanel;
    private javax.swing.JLabel Logo;
    private javax.swing.JPanel MenuP;
    private javax.swing.JButton btCustomer;
    private javax.swing.JButton btEmployee;
    private javax.swing.JButton btINOUT;
    private javax.swing.JButton btOrder;
    private javax.swing.JButton btProduct;
    private javax.swing.JButton btReport;
    private javax.swing.JButton btStock;
    private javax.swing.JButton btUser;
    private javax.swing.JButton btlogout;
    private javax.swing.JButton btpos;
    private javax.swing.JButton btsalary;
    private javax.swing.JLabel iconBer;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JScrollPane srcMain;
    private javax.swing.JLabel txtName;
    private javax.swing.JLabel txtNameBar;
    private javax.swing.JLabel txtrole;
    // End of variables declaration//GEN-END:variables
}
