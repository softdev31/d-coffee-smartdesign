/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package Order;

import model.Product;

/**
 *
 * @author ASUS
 */
public interface OnBuyProductListener {
    public void buy(Product product,int amount,String size,String sweet);
}
