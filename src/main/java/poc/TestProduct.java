/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package poc;

import Dao.ProductDao;
import DatabaseHelper.DatabaseHelper;
import model.Product;

/**
 *
 * @author 66986
 */
public class TestProduct {

    public static void main(String[] args) {

        ProductDao productDao = new ProductDao();

        System.out.println("---------------getAll---------------");
        for (Product p : productDao.getAll()) {
            System.out.println(p);
        }
//
//        System.out.println("---------------getOne---------------");
//        Product pd = productDao.get(11130);
//        System.out.println(pd);
//
//        //  System.out.println("---------------save---------------");
//        //  Product newProduct = new Product("ชามะนาว", "S", 0, 45, 1, "ชามะนาว.jpg");
//        //  Product insertedProduct = productDao.save(newProduct);
//        //  System.out.println(insertedProduct);
//        // insertedProduct.setSize("L");
////            System.out.println("---------------update---------------");
////            pd.setSize("NS");
////            productDao.update(pd);
////            Product updateproduct = productDao.get(pd.getId());
////            System.out.println(updateproduct);
//        System.out.println("---------------delete---------------");
//        productDao.delete(pd);
//        for (Product p : productDao.getAll()) {
//            System.out.println(p);
//
//        }

//            DatabaseHelper.close();
//
//        
//        
        }
        
}
