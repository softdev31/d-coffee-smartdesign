/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package poc;

import Dao.ProductCategoryDao;
import DatabaseHelper.DatabaseHelper;
import model.ProductCategory;

/**
 *
 * @author 66986
 */
public class TestProductCategory {
    public static void main(String[] args) {
        ProductCategoryDao productCategoryDao = new ProductCategoryDao();

        System.out.println("---------------getAll---------------");
        for (ProductCategory p : productCategoryDao.getAll()) {
            System.out.println(p);
            }
        System.out.println("---------------getOne---------------");
        ProductCategory pcr = productCategoryDao.get(5);
        System.out.println(pcr);
        
//        System.out.println("---------------save---------------");
//        ProductCategory newProductCategory = new ProductCategory("อุปกรณ์");
//        ProductCategory insertedProduct = productCategoryDao.save(newProductCategory);
//        System.out.println(insertedProduct);
        //   insertedProduct.setName(name);
        
//        System.out.println("---------------update---------------");
//        pcr.setName("ขนมไทย");
//        productCategoryDao.update(pcr);
//        ProductCategory updateProduct = productCategoryDao.get(pcr.getId());
//        System.out.println(updateProduct);
    
        System.out.println("---------------delete---------------");
        productCategoryDao.delete(pcr);
        for (ProductCategory p : productCategoryDao.getAll()) {
            System.out.println(p);

        }
        DatabaseHelper.close();
    

    }
}
