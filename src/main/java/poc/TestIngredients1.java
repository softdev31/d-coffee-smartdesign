/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package poc;

import Dao.EmployeeDao;
import Dao.IngredientsDao;
import model.Employee;
import model.Ingredients;

/**
 *
 * @author Rattanalak
 */
public class TestIngredients1 {
    public static void main(String[] args) {
        IngredientsDao ingredientsDao = new IngredientsDao();
         for(Ingredients i : ingredientsDao.getAll()){
             System.out.println(i);
         }
         
         System.out.println("-------------------------------------");
          Ingredients in1 = ingredientsDao.get(11222);
        System.out.println(in1);
//        System.out.println("-------------------------------------");
//        EmployeeDao empDao = new EmployeeDao();
//        Employee emp = empDao.get(12);
//        Ingredients in2 = new Ingredients("น้ำหวาน","ลิตร" , 15, 15, 15, "12/10/2565",emp );
//        Ingredients newIn = ingredientsDao.save(in2);
//        System.err.println(newIn);
        
         in1.setUnit("แกลลอน");
         System.out.println(in1);
        ingredientsDao.update(in1);
        Ingredients updateIn = ingredientsDao.get(in1.getId());
        System.out.println(updateIn);
        
        System.out.println("-------------------------------------");

        ingredientsDao.delete(in1);
        for (Ingredients i : ingredientsDao.getAll()) {
            System.out.println(i);
        }
    }
}
