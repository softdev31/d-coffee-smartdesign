/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package poc;

import Dao.CustomerDao;
import DatabaseHelper.DatabaseHelper;
import model.Customer;

/**
 *
 * @author OS
 */
public class TestCustomerDao {

    public static void main(String[] args) {
        CustomerDao cusDao = new CustomerDao();

        System.out.println("---------------getAll---------------");
        for (Customer c : cusDao.getAll()) {
            System.out.println(c);
        }

//        System.out.println("---------------getOne---------------");
//        Customer cus1 = cusDao.get(88532);
//        System.out.println(cus1);
//        
//        System.out.println("---------------save---------------");
//        Customer newCus = new Customer(88541, "นางสาวดีอา", "แกรนด์เด", "096-007-8497", 20);
//        Customer insertedCus = cusDao.save(newCus);
//        System.out.println(insertedCus);

//        System.out.println("---------------update---------------");
//        Customer cus1 = cusDao.get(88541);
//        cus1.setFirstname("นางสาวเดอา");
//        cusDao.update(cus1);
//        Customer updateCus = cusDao.get(cus1.getId());
//        System.out.println(updateCus);

//        System.out.println("---------------delete---------------");
//        Customer cus1 = cusDao.get(88541);
//        cusDao.delete(cus1);
//        for (Customer u : cusDao.getAll()) {
//            System.out.println(u);
//        }

        System.out.println("---------------getAllWhere---------------");
        for (Customer c : cusDao.getAll(" cus_firstname like 'u%' ", " cus_firstname asc, cus_lastname desc ")) {
            System.out.println(c);
        }

        DatabaseHelper.close();

    }

}
